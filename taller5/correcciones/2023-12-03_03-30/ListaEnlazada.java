package aed;

public class ListaEnlazada<T>{
    private Nodo _first;
    private Nodo _last;
    private int _longitud;

    private class Nodo {
        Nodo sig;
        Nodo ant;
        T valor;

        public Nodo(T v) {
            valor = v;
        }
        
    }

    public ListaEnlazada() {
        _first = null;
        _last = null;
        _longitud = 0;
    }

    public int longitud() {
        return _longitud;
    }

    public void agregarAdelante(T elem) {
       Nodo nuevoNodo = new Nodo(elem);
       
       if(_first == null){
        _first = nuevoNodo;
        _last = nuevoNodo;
       } else {
        nuevoNodo.sig = _first;
        _first.ant = nuevoNodo;
        _first = nuevoNodo;
       }
       _longitud++;
    }

    public void agregarAtras(T elem) {
        Nodo nuevoNodo = new Nodo(elem);
        if(_last == null) {
            _last = nuevoNodo;
            _first = nuevoNodo;
        } else {
            nuevoNodo.ant = _last;
            _last.sig = nuevoNodo;
            _last = nuevoNodo;
        }
        _longitud++;
    }

    public T obtener(int i) {
        Nodo actual = _first;
        if(i<_longitud) {
            for (int j=0;j<i;j++) {
                actual = actual.sig;
            }
        
        }
        return actual.valor;
    }

    public void eliminar(int i) {
        Nodo actual = _first;
        if(i<_longitud) {
            for(int j=0; j<i;j++) {
                actual = actual.sig;
            }
        }
        if(_longitud > 1) {
            if(actual == _first) {
                actual = actual.sig;
                actual.ant = null;
                _first = actual;
            }else if(actual == _last) {
                actual = actual.ant;
                actual.sig = null;
                _last = actual;
            } else {
                actual.ant.sig = actual.sig;
                actual.sig.ant = actual.ant;
            }
        } else {
            actual.sig = null;
            actual.ant = null;
            _first = null;
            _last = null;
        }
        _longitud--;
    }

    public void modificarPosicion(int indice, T elem) {
        Nodo actual = _first;
        if(indice <_longitud) {
            for(int i=0; i<indice;i++) {
                actual = actual.sig;
            }
        }
        actual.valor = elem;
    }

    public ListaEnlazada<T> copiar() {
        ListaEnlazada<T> lista = new ListaEnlazada<>();
        Nodo actual = _first;
        for(int i=0;i<_longitud;i++) {
            lista.agregarAtras(actual.valor);
            actual = actual.sig;
        }
        return lista;
    }

    public ListaEnlazada(ListaEnlazada<T> lista) {
        _first = lista.copiar()._first;
        _last = lista.copiar()._last;
        _longitud = lista.copiar()._longitud;
    }
    
    @Override
    public String toString() {
        Nodo actual = _first;
        String res = "[";
        for(int i=0; i<_longitud;i++) {
            if(actual.sig == null) {
                res = res + actual.valor;
            } else {
                res = res + actual.valor + ", ";
                actual = actual.sig;
            }
        }
        res = res + "]";
        return res;
    }

    public void unir(ListaEnlazada<T> lista) {

        if(lista._first == null) {
            lista._first = _first;
            lista._last = _last;

        } else if (_first == null) {
            _first = lista._first;
            _last = lista._last;

        } else {
            _last.sig = lista._first;
            lista._first.ant = _last;

            _last = lista._last;
            lista._first = _first;
        }
        this._longitud += lista._longitud;
    }

    private class ListaIterador implements Iterador<T> {
        private Nodo anterior;
        private Nodo siguiente;

        public ListaIterador(){
            anterior = null;
            siguiente = _first;
        }


        public boolean haySiguiente() {
	        return siguiente != null;
        }
        
        public boolean hayAnterior() {
	        return anterior != null;
        }

        public T siguiente() {
            T res = siguiente.valor;
            anterior = siguiente;
            siguiente = siguiente.sig;
            return res;
        }
        

        public T anterior() {
	        T res = anterior.valor;
            siguiente = anterior;
            anterior = anterior.ant;
            return res;
        }
    }

    public Iterador<T> iterador() {
	    return new ListaIterador();
    }

}
