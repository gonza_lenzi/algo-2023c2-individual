package aed;

public class InternetToolkit {
    public InternetToolkit() {
    }

    public Fragment[] tcpReorder(Fragment[] fragments) { //insertion sort
        int comparacion;
        for(int i=1; i < fragments.length; i++) {
            Fragment valor = fragments[i];
            int k = i;
            for(int j=i-1; j >=0; j--) {
                comparacion = valor.compareTo(fragments[j]);

                if(comparacion < 0) {
                    fragments[k] = fragments[j];
                    k--;

                    if(j == 0) {
                        fragments[k] = valor;
                    }

                } else {
                    fragments[k] = valor;
                    j = -1;
                }

            }
        }

        return fragments;
    }

    public Router[] kTopRouters(Router[] routers, int k, int umbral) {
        Heap heap = new Heap(routers.length);
        Router[] trafic = new Router[k];

        heap.array2Heap(routers); //O(n)
        
        int j = 0;
        while( j < k) {
            Router max = heap.getMax();
            
            if(max.getTrafico() > umbral) {
                trafic[j] = max;
                heap.desencolarReordenando();
            }

            j++;
        }

        return trafic;
    }

    public IPv4Address[] sortIPv4(String[] ipv4) { //Radix sort
        IPv4Address[] res = new IPv4Address[ipv4.length];

        for(int i = 0; i < ipv4.length; i++) {
            res[i] = new IPv4Address(ipv4[i]);
        }


        ListaEnlazada<IPv4Address>[] buckets = new ListaEnlazada[256];

        for(int i=3; i>=0; i--) {

            for(int j=0; j<256;j++) {
                buckets[j] = new ListaEnlazada<IPv4Address>();

            }

            for(int k=0; k < res.length; k++) {
                IPv4Address ip = res[k];
                buckets[ip.getOctet(i)].agregarAtras(ip);
            }

            ListaEnlazada<IPv4Address> l = new ListaEnlazada<IPv4Address>();
            
            for(int h=0; h < 256; h++) {
                l.unir(buckets[h]);
            }

            Iterador<IPv4Address> iterador = l.iterador();
            int s=0;
            while(iterador.haySiguiente()) {
                res[s] = iterador.siguiente();
                s++;
            }

        }

        for(int i=0; i<res.length; i++){
            System.out.println(res[i].toString());
        }

        return res;
    }

}
