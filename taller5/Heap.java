package aed;

public class Heap {
    private int longi;
    private Router[] heap;

    public Heap(int l) {
        heap = new Router[l];
        longi = 0;
    }

    public void array2Heap(Router[] arr) {
        for(int i=0; i< arr.length; i++) {
            heap[i] = arr[i];
            longi ++;
        }

        int j = longi/2 - 1;
        
        while(j >= 0) {

            reordenarDesde(j);

            j--;
        }
    }

    public void reordenarDesde(int i) {
        int izq = 2*i + 1,
            der = 2*i + 2;
        

        if(der >= longi && izq < longi) {

            if(heap[i].compareTo(heap[izq]) < 0) {
                swap(i, izq);
                reordenarDesde(izq);
            }

        } else if(der < longi) {
            int comparacion = heap[izq].compareTo(heap[der]);

            if(comparacion < 0 && heap[i].compareTo(heap[der]) < 0) {
                
                swap(i, der);
                reordenarDesde(der);

            } else {

                if(heap[i].compareTo(heap[izq]) < 0) {

                    swap(i, izq);
                    reordenarDesde(izq);

                }
                
            }
        }
    }

    public void swap(int i, int j) {
        Router v = heap[i];
        heap[i] = heap[j];
        heap[j] = v;
    }

    public int length() {
        return longi;
    }

    public Router getMax() {
        return heap[0];
    }

    public void desencolarReordenando() {
        longi --;
        swap(0,longi);
        Router[] newHeap = new Router[longi];

        for(int i=0; i < longi; i++) {
            newHeap[i] = heap[i];
        }

        heap = newHeap;

        reordenarDesde(0);

    }
}


