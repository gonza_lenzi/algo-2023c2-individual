package aed;

import java.util.*;

// Todos los tipos de datos "Comparables" tienen el método compareTo()
// elem1.compareTo(elem2) devuelve un entero. Si es mayor a 0, entonces elem1 > elem2
public class ABB<T extends Comparable<T>> implements Conjunto<T> {
    private Nodo _raiz;
    private int _cardinal;

    private class Nodo {
        private T value;
        private Nodo der;
        private Nodo izq;
        private Nodo padre;

        public Nodo(T v) {
            value = v;
            der = null;
            izq = null;
            padre = null;
        }
    }

    public ABB() {
        _raiz = null;
        _cardinal = 0;
    }

    public int cardinal() {
        return _cardinal;   
    }

    public T minimo(){
        Nodo actual = _raiz;
        while (actual.izq != null) {
            actual = actual.izq;
        }
        return actual.value;
    }

    public T maximo(){
        Nodo actual = _raiz;
        while (actual.der != null) {
            actual = actual.der;
        }
        return actual.value;
    }

    public void insertar(T elem){

        Nodo nuevoNodo = new Nodo(elem);
        if(!(pertenece(elem))) {


            if(_raiz == null) {

                _raiz = nuevoNodo;

            } else {

                Nodo actual = _raiz;
                int comparacion = elem.compareTo(actual.value);
                
                while(comparacion != 0){
                    
                    if(comparacion < 0) {
                        if(actual.izq  == null) {
                            actual.izq = nuevoNodo;
                            nuevoNodo.padre = actual;
                        }
                        actual = actual.izq;
                    } else if(comparacion > 0){
                        if(actual.der == null) {
                            actual.der = nuevoNodo;
                            nuevoNodo.padre = actual;
                        }
                        actual = actual.der;
                    }
                    comparacion = elem.compareTo(actual.value);
                }
            }

            _cardinal ++;
        }
    }

    public boolean pertenece(T elem){
        boolean res = false;

        if(_raiz != null) {
        
            Nodo actual = _raiz;
            
            while((res == false && actual != null)) {
                int comparacion = elem.compareTo(actual.value);

                if(comparacion > 0) {
                    actual = actual.der;
                } else if(comparacion < 0) {
                    actual = actual.izq;
                } else {
                    res = true;
                }
            }
        }
        return res;
    }


    public void eliminar(T elem){
        Nodo nodo = buscarNodo(elem);

        if(nodo != null) {
            _cardinal --;

            if(nodo.izq != null) {
                if(nodo.der != null) {
                    Nodo predecesor = buscarPredecesor(nodo);

                    eliminar(predecesor.value);
                    _cardinal++;

                    nodo.value = predecesor.value;
                } else {
    
                    if(nodo.padre != null) {
                        padreApuntaA(nodo, nodo.izq);
                    } else {
                        _raiz = nodo.izq;
                    }
                    nodo.izq.padre = nodo.padre;
                }

            } else {

                if(nodo.padre != null) {
                    padreApuntaA(nodo, nodo.der);
                } else {
                    _raiz = nodo.der;
                }

                if(nodo.der != null) {
                    nodo.der.padre = nodo.padre;
                }
            }
        }
    }

    private void padreApuntaA(Nodo hijo, Nodo nuevoHijo) {
        if(hijo != null) {
            if(hijo.padre.izq != null && hijo.padre.izq == hijo) {
                hijo.padre.izq = nuevoHijo;
            } else {
                hijo.padre.der = nuevoHijo;
            }
        }
    }

    private Nodo buscarNodo(T elem) {
        Nodo actual = _raiz;
    
        while (actual != null) {
            int comparacion = elem.compareTo(actual.value);
    
            if (comparacion == 0) {
                return actual; // Elemento encontrado
            } else if (comparacion > 0) {
                actual = actual.der;
            } else {
                actual = actual.izq;
            }
        }
    
        return null;
    }

    private Nodo buscarPredecesor(Nodo nodo) {
        Nodo buscador = nodo.izq;

        while(buscador.der != null) {
             buscador = buscador.der;
        }

        return buscador;
    }


    public String toString(){
        String res = "{";
        Nodo actual;

        if(_raiz != null) {
            actual = _raiz;

            while(actual.izq != null) {
                actual = actual.izq;
            }

            while(actual.value != maximo()) {
                res = res + actual.value + ",";
                if(actual.der != null) {
                    actual = actual.der;
                    while (actual.izq != null) {
                        actual = actual.izq;
                    }
                } else {
                    if(actual.value.compareTo(actual.padre.value) < 0) {
                        actual = actual.padre;
                    } else {
                        actual = actual.padre.padre;
                    }
                }
            }

            res = res + actual.value;

        }

        res = res + "}";
        return res;
    }

    private class ABB_Iterador implements Iterador<T> {
        private Nodo actual;

    public ABB_Iterador() {
        actual = _raiz;
        while (actual != null && actual.izq != null) {
            actual = actual.izq;
        }
    }

    public boolean haySiguiente() {
        return actual != null;
    }

    public T siguiente() {
        T valor = actual.value;

        if (actual.der != null) {
            actual = actual.der;
            while (actual.izq != null) {
                actual = actual.izq;
            }
        } else {
            Nodo padre = actual.padre;
            while (padre != null && actual == padre.der) {
                actual = padre;
                padre = actual.padre;
            }
            actual = padre;
        }

        return valor;
    }
    }

    public Iterador<T> iterador() {
        return new ABB_Iterador();
    }

}
