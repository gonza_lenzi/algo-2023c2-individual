package aed;

import java.util.Scanner;
import java.io.PrintStream;

class Archivos {
    float[] leerVector(Scanner entrada, int largo) {
        float[] res = new float[largo];

        for (int i = 0; i < largo; i++) {
            res[i] = entrada.nextFloat();
        }
        return res;
    }

    float[][] leerMatriz(Scanner entrada, int filas, int columnas) {
        float[][] res = new float[filas][columnas];

        for (int i = 0; i < filas; i++) {
            for (int j=0; j < columnas; j++) {
                res[i][j] = entrada.nextFloat();
            }
        }
        return res;
    }

    String generarEspacios(int cantidad) {
        String espacios = "";
        for (int i=0; i < cantidad; i++) {
            espacios = espacios + " "; 
        }
        return espacios;
    }

    String generarAsteriscos(int cantidad) {
        String asteriscos = "";
        for (int i=0; i < cantidad; i++) {
            asteriscos = asteriscos + "*";
        }
        return asteriscos;
    }

    void imprimirPiramide(PrintStream salida, int alto) {
        int basePiramide = (alto*2) - 1;

        for (int i=0; i < alto; i++) {
            int cantAsteriscos = 2*i+1;
            int cantEspacios = (basePiramide - cantAsteriscos)/2;

            String escalonPiramide = generarEspacios(cantEspacios) + generarAsteriscos(cantAsteriscos) + generarEspacios(cantEspacios) + "\n";

            salida.print(escalonPiramide);
        }
    }

    void imprimirPiramide2(PrintStream salida, int alto) {
        int basePiramide = (alto*2) - 1;

        
    }
}
