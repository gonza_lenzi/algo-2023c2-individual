package aed;

class Funciones {
    int cuadrado(int x) {
        int res = x*x;
        return res;
    }

    double distancia(double x, double y) {
        double res = Math.sqrt((x*x)+(y*y));
        return res;
    }

    boolean esPar(int n) {
        boolean res = n % 2 == 0;
        return res;
    }

    boolean esBisiesto(int n) {
        boolean res = ((n % 400 == 0) || (n % 4 == 0 && n % 100 != 0));
        return res;
    }

    int factorialIterativo(int n) {
        int res = 1;
        for (int i = n; i > 0; i = i -1 ) {
            res = res*i;   
        }
        return res;
    }

    int factorialRecursivo(int n) {
        int res;
        if (n == 0) {
            res = 1;
        } else {
            res = factorialRecursivo(n-1)*n;
        }
        return res;
    }

    boolean esPrimo(int n) {
        boolean res = true;
        for (int i = 2; i < n; i++) {
            if (n % i == 0) {
                res = false;
            }
        }
        if (n == 0 || n == 1) {
            res = false;
        }
        return res;
    }

    int sumatoria(int[] numeros) {
        int res = 0;
        for (int x : numeros) {
            res = res + x;
        }
        return res;
    }

    int busqueda(int[] numeros, int buscado) {
        int res = (-1);
        for (int i = 0; i < numeros.length; i++) {
            if (numeros[i] == buscado) {
                res = i;
            }
        }
        return res;
    }

    boolean tienePrimo(int[] numeros) {
        boolean res = false;
        for (int i = 0; i < numeros.length; i++) {
            if (esPrimo(numeros[i])) {
                res = true;
            }
        }

        return res;
    }

    boolean todosPares(int[] numeros) {
        boolean res = true;
        for (int i = 0; i < numeros.length; i++) {
            if (!(esPar(numeros[i]))) {
                res = false;
            }
        }
        return res;
    }

    boolean esPrefijo(String s1, String s2) {
        boolean res = true;
        if (s1.length() > s2.length()) {
            res = false;
        } else {
            for (int i = 0; i < s1.length(); i++) {
                if ((s1.charAt(i) != s2.charAt(i))) {
                    res = false;
                }
            }
        }    
            return res;
    }

    boolean esSufijo(String s1, String s2) {
        boolean res = true;
        if (s1.length() > s2.length()) {
            res = false;
        } else {
            for (int i = 0; i < s1.length(); i++) {
                if (s1.charAt((s1.length() -i) -1 ) != s2.charAt((s2.length() -i) - 1)) {
                    res = false;
                }
            }
        }       
        return res;
    }
}
